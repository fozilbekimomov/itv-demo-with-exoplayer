package uz.ictandroid.itv.network

import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import uz.ictandroid.itv.utils.BASE_URL

/**
 * Created by Fozilbek on 6/12/2019 for iTV
 **/
object RetrofitClient {
    private var instanceNoUsable: Retrofit? = null
    val instance: Retrofit
        get() {
            if (instanceNoUsable == null) {
                instanceNoUsable = Retrofit.Builder().baseUrl(BASE_URL)
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(ScalarsConverterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
            }
            return instanceNoUsable!!
        }

}