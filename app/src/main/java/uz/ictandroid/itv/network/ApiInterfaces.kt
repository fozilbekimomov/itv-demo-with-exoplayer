package uz.ictandroid.itv.network

import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import uz.ictandroid.itv.models.MovieListResponce
import uz.ictandroid.itv.models.MovieShowSingleResponce

/**
 * Created by Fozilbek on 6/12/2019 for iTV
 **/
interface ApiInterfaces {
    @GET("api/content/main/2/list")
    fun getAllMoviesAsList(
        @Query("user") token: String
    ): Observable<MovieListResponce>

    @GET("api/content/main/2/show/{id}")
    fun getMovieShowSingle(
        @Path("id") id: Long,
        @Query("user") apiKey: String
    ): Observable<MovieShowSingleResponce>

    @GET("api/content/main/2/list")
    fun getAllMoviesAsPagingList(
        @Query("page") page: Int,
        @Query("user") token: String
    ): Observable<MovieListResponce>
}