package uz.ictandroid.itv.ui.main

import io.reactivex.disposables.Disposable
import uz.ictandroid.itv.models.Movie
import uz.ictandroid.itv.network.ApiInterfaces

/**
 * Created by Fozilbek on 6/13/2019 for iTV
 **/
interface MainMvp {
    interface viewList {
        fun onMoviesListReady(movie: ArrayList<Movie>)
        fun onErrorOccurAllList(message: String?)
    }

    interface viewAsPageList{
        fun onMoviesPageListReady(movie: ArrayList<Movie>)
        fun onErrorOccurPageList(message: String?)
    }

    interface presenter {
        fun getMoviesList(apiList: ApiInterfaces): Disposable
        fun getMoviesListAsPage(page: Int, apiList: ApiInterfaces): Disposable
    }


}