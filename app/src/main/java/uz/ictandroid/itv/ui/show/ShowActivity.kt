package uz.ictandroid.itv.ui.show

import android.app.Dialog
import android.content.res.Configuration
import android.net.Uri
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.text.Html
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.view.WindowManager
import android.view.animation.AccelerateInterpolator
import android.view.animation.AnticipateOvershootInterpolator
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.google.android.exoplayer2.DefaultLoadControl
import com.google.android.exoplayer2.DefaultRenderersFactory
import com.google.android.exoplayer2.ExoPlayerFactory
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.source.ExtractorMediaSource
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.source.dash.DashMediaSource
import com.google.android.exoplayer2.source.dash.DefaultDashChunkSource
import com.google.android.exoplayer2.source.hls.HlsMediaSource
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_show.*
import kotlinx.android.synthetic.main.preview_header_content.*
import kotlinx.android.synthetic.main.preview_header_image.*
import uz.ictandroid.itv.R
import uz.ictandroid.itv.adapters.MoviePageListAdapter
import uz.ictandroid.itv.adapters.SnapshotsAdapter
import uz.ictandroid.itv.models.Movie
import uz.ictandroid.itv.models.MovieParams
import uz.ictandroid.itv.models.Snapshot
import uz.ictandroid.itv.network.ApiInterfaces
import uz.ictandroid.itv.network.RetrofitClient
import uz.ictandroid.itv.utils.logi

class ShowActivity : AppCompatActivity(), ShowMvp.view {

    lateinit var presenter: ShowPresenter
    var movieID: Long? = null
    lateinit var apiList: ApiInterfaces
    private val disposable = CompositeDisposable()

    private val STATE_RESUME_WINDOW = "resumeWindow"
    private val STATE_RESUME_POSITION = "resumePosition"
    private val STATE_PLAYER_FULLSCREEN = "playerFullscreen"

    private var videoSource: MediaSource? = null
    private var exoPlayerFullscreen = false
    private var fullScreenButton: FrameLayout? = null
    private var fullScreenIcon: ImageView? = null
    private var fullScreenDialog: Dialog? = null

    private var resumeWindow: Int = 0
    private var resumePosition: Long = 0
    lateinit var movie: Movie
    private var actorsView: TextView? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.activity_show)
        presenter = ShowPresenter(this)

        if (savedInstanceState != null) {
            resumeWindow = savedInstanceState.getInt(STATE_RESUME_WINDOW)
            resumePosition = savedInstanceState.getLong(STATE_RESUME_POSITION)
            exoPlayerFullscreen = savedInstanceState.getBoolean(STATE_PLAYER_FULLSCREEN)
            movie = savedInstanceState.getSerializable("movie") as Movie
        }

        setUpUi()

    }

    private fun setUpUi() {
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp)
        setSupportActionBar(toolbar)
        toolbar.setNavigationOnClickListener { onBackPressed() }
        supportActionBar!!.title = ""

        apiList = RetrofitClient.instance.create(ApiInterfaces::class.java)
        movieID = intent.extras.getLong("id")
        logi(this, "received movieid $movieID")
        disposable.add(presenter.getMovie(movieID!!, apiList))
    }


    override fun onMovieReady(movie: Movie) {
        this.movie = movie
        logi(this, "onMovieReady")
        Glide.with(this).load(movie.files.poster_url).into(imageViewPreview)
        textViewCountry.text = movie.countries_str
        textViewYear.text = "${movie.year}"
        textViewGenre.text = movie.genres_str
        textViewTitle.text = movie.title
        textViewDescription.text = Html.fromHtml(movie.description)
        actorsView = findViewById(R.id.textViewActors)
        var actors = "Actors: "
        for (i in 0 until movie.actors!!.size) {
            logi(this, movie.actors[i].name)
            actors=actors.plus(",").plus(movie.actors[i].name)
        }

        actorsView?.text = actors

        recyclerViewMovies.layoutManager = GridLayoutManager(this, 2)
        val movieAdapter = MoviePageListAdapter(this, movie.movies!!, null)
        recyclerViewMovies.adapter = movieAdapter

//        imageViewPlay.setOnClickListener {
//            logi(this, " imageViewPlay.setOnClickListener")
//        }


    }

    fun PlayVideo(view: View) {
        logi(this, " imageViewPlay.setOnClickListener")
        headerContent.animate().translationY(800f).setDuration(400).setInterpolator(AccelerateInterpolator())
            .start()
        imageViewPlay.animate().scaleX(0f).scaleY(0f).setDuration(1000)
            .setInterpolator(AnticipateOvershootInterpolator()).start()
        exoPlayerView.visibility = View.VISIBLE

        playVideo()
    }

    override fun onSnapshotsReady(snapshots: ArrayList<Snapshot>) {
        recyclerViewSnapshots.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        recyclerViewSnapshots.adapter = SnapshotsAdapter(this, snapshots)
    }

    override fun onErrorOccured(message: String?) {
        Snackbar.make(parentLayout, "${message}", Snackbar.LENGTH_SHORT).show()
        logi(this, "$message")
    }


    public override fun onSaveInstanceState(outState: Bundle) {
        logi(this, "onSaveInstanceState")
        outState.putInt(STATE_RESUME_WINDOW, resumeWindow)
        outState.putLong(STATE_RESUME_POSITION, resumePosition)
        outState.putBoolean(STATE_PLAYER_FULLSCREEN, exoPlayerFullscreen)
        outState.putSerializable("movie", movie)
        super.onSaveInstanceState(outState)
    }

    override fun onDestroy() {
        super.onDestroy()
        // UNSUBSCRIBE
        disposable.dispose()
    }


    private fun initFullscreenDialog() {

        fullScreenDialog = object : Dialog(this, android.R.style.Theme_Black_NoTitleBar_Fullscreen) {
            override fun onBackPressed() {
                if (exoPlayerFullscreen)
                    closeFullscreenDialog()
                super.onBackPressed()
            }
        }
    }


    private fun openFullscreenDialog() {

        (exoPlayerView.parent as ViewGroup).removeView(exoPlayerView)
        fullScreenDialog!!.addContentView(
            exoPlayerView,
            ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        )
        fullScreenIcon!!.setImageDrawable(
            ContextCompat.getDrawable(
                this@ShowActivity,
                R.drawable.ic_fullscreen_skrink
            )
        )
        exoPlayerFullscreen = true
        fullScreenDialog!!.show()

    }


    private fun closeFullscreenDialog() {

        (exoPlayerView.parent as ViewGroup).removeView(exoPlayerView)
        main_media_frame.addView(exoPlayerView)
        exoPlayerFullscreen = false
        fullScreenDialog!!.dismiss()
        fullScreenIcon!!.setImageDrawable(
            ContextCompat.getDrawable(
                this@ShowActivity,
                R.drawable.ic_fullscreen_expand
            )
        )

    }


    private fun initFullscreenButton() {

        val controlView = exoPlayerView.findViewById<View>(R.id.exo_controller)
        fullScreenIcon = controlView.findViewById(R.id.exo_fullscreen_icon)
        fullScreenButton = controlView.findViewById(R.id.exo_fullscreen_button)
        fullScreenButton!!.setOnClickListener {
            if (!exoPlayerFullscreen)
                openFullscreenDialog()
            else
                closeFullscreenDialog()
        }
    }


    private fun initExoPlayer() {

        val bandwidthMeter = DefaultBandwidthMeter()
        val videoTrackSelectionFactory = AdaptiveTrackSelection.Factory(bandwidthMeter)
        val trackSelector = DefaultTrackSelector(videoTrackSelectionFactory)
        val loadControl = DefaultLoadControl()
        val player = ExoPlayerFactory.newSimpleInstance(DefaultRenderersFactory(this), trackSelector, loadControl)
        exoPlayerView.player = player

        logi(this, "init ${resumePosition}    ${resumeWindow}")

        (exoPlayerView.player as SimpleExoPlayer).prepare(videoSource)
        exoPlayerView.player.playWhenReady = true
        exoPlayerView.player.seekTo(resumeWindow, resumePosition)
    }

    fun playVideo() {
        imageViewPlay.visibility = View.GONE
        imageViewPreview.setImageResource(R.color.colorPrimaryDark)

        var source: String = getSourceFromMovie(movie.params)
        if (source.equals("https://storage2.itv.uz/hls/ftp1/converted%2FVideo%2Fdrama/DreamKeeper.2003.DVDRip.2.mp4/index.m3u8?s=yo9R9AhBQ5eVcSnxXyCb8g&e=1560425912")) {
            Snackbar.make(parentLayout, "Media not found. Default media is playing", Snackbar.LENGTH_INDEFINITE).show()
        }

        if (videoSource == null) {
            logi(this, "videoSourceNull")
            initFullscreenDialog()
            initFullscreenButton()
            videoSource =
                buildMediaSource(Uri.parse(source))
        }


        initExoPlayer()

        if (exoPlayerFullscreen) {
            (exoPlayerView.parent as ViewGroup).removeView(exoPlayerView)
            fullScreenDialog!!.addContentView(
                exoPlayerView,
                ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
            )
            fullScreenIcon!!.setImageDrawable(
                ContextCompat.getDrawable(
                    this@ShowActivity,
                    R.drawable.ic_fullscreen_skrink
                )
            )
            fullScreenDialog!!.show()
        }
    }

    private fun getSourceFromMovie(params: MovieParams): String {
        var source ="https://storage2.itv.uz/hls/ftp1/converted%2FVideo%2Fdrama/DreamKeeper.2003.DVDRip.2.mp4/index.m3u8?s=yo9R9AhBQ5eVcSnxXyCb8g&e=1560425912"
        if (params.is_hd) {
//            logi(this, movie.files.video_hd!!.video_url!!)
            movie.files.video_hd!!.video_url?.let {
                source = ""
                source = it
            }

        } else if (params.is_4k) {
            movie.files.video_4k!!.video_url?.let {
                source = ""
                source = it
            }
        } else if (params.is_3d) {
            movie.files.video_3d!!.video_url?.let {
                source = ""
                source = it
            }
        } else if (params.is_sd) {
            movie.files.video_sd!!.video_url?.let {
                source = ""
                source = it
            }
        }

        return source
    }

    private fun buildMediaSource(uri: Uri): MediaSource {

        val userAgent = "exoplayer-lazy"

        return if (uri.lastPathSegment.contains("mp3") || uri.lastPathSegment.contains("mp4")) {
            ExtractorMediaSource.Factory(DefaultHttpDataSourceFactory(userAgent))
                .createMediaSource(uri)
        } else if (uri.lastPathSegment.contains("m3u8")) {
            HlsMediaSource.Factory(DefaultHttpDataSourceFactory(userAgent))
                .createMediaSource(uri)
        } else {
            val dashChunkSourceFactory = DefaultDashChunkSource.Factory(
                DefaultHttpDataSourceFactory("ua", null)
            )
            val manifestDataSourceFactory = DefaultHttpDataSourceFactory(userAgent)
            DashMediaSource.Factory(dashChunkSourceFactory, manifestDataSourceFactory).createMediaSource(uri)
        }
    }

    override fun onConfigurationChanged(newConfig: Configuration?) {
        super.onConfigurationChanged(newConfig)

        if (newConfig!!.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            openFullscreenDialog()
            exoPlayerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL)
        } else {
            exoPlayerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FIT)
        }

    }

    override fun onResume() {
        super.onResume()
        if (resumePosition > 0) {
            playVideo()
        }
    }

    override fun onPause() {
        super.onPause()
       logi(this,"Pause")


        if (exoPlayerView != null && exoPlayerView.player != null) {
            resumeWindow = exoPlayerView.player.currentWindowIndex
            resumePosition = Math.max(0, exoPlayerView.player.contentPosition)

            logi(this, "${resumePosition}    ${resumeWindow}")
            exoPlayerView.player.release()
        }

        if (fullScreenDialog != null)
            fullScreenDialog!!.dismiss()
    }


}
