package uz.ictandroid.itv.ui.main

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import uz.ictandroid.itv.network.ApiInterfaces
import uz.ictandroid.itv.utils.logi
import uz.ictandroid.itv.utils.token

/**
 * Created by Fozilbek on 6/13/2019 for iTV
 **/
class MainPresenter(val viewList: MainMvp.viewList, val viewPageList: MainMvp.viewAsPageList) : MainMvp.presenter {
    override fun getMoviesList(apiList: ApiInterfaces): Disposable =
        apiList.getAllMoviesAsList(token).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(
            {
                viewList.onMoviesListReady(it.data.movies)
            },
            {
                viewList.onErrorOccurAllList(it.message)
                logi(this, "${it.message}")
            })

    override fun getMoviesListAsPage(page: Int, apiList: ApiInterfaces): Disposable =
        apiList.getAllMoviesAsPagingList(
            page,
            token
        ).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(
            {
                viewPageList.onMoviesPageListReady(it.data.movies)
            }, {
                viewPageList.onErrorOccurPageList(it.message)
                logi(this, "${it.message}")

            })
}