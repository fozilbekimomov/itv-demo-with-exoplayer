package uz.ictandroid.itv.ui.main

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_main.*
import uz.ictandroid.itv.R
import uz.ictandroid.itv.adapters.MovieListAdapter
import uz.ictandroid.itv.adapters.MoviePageListAdapter
import uz.ictandroid.itv.models.Movie
import uz.ictandroid.itv.network.ApiInterfaces
import uz.ictandroid.itv.network.RetrofitClient
import uz.ictandroid.itv.ui.BaseActivity
import uz.ictandroid.itv.utils.checkNetworkConnection

class MainActivity : BaseActivity(), MainMvp.viewList, MainMvp.viewAsPageList, MoviePageListAdapter.OnScrolledToBottom {
    override fun setLayoutId(): Int = R.layout.activity_main

    lateinit var presenter: MainPresenter
    lateinit var apiList: ApiInterfaces
    private val disposableList = CompositeDisposable()
    private val disposablePageList = CompositeDisposable()
    private var page = 2
    var listMovie = ArrayList<Movie>()
    var listMoviePage = ArrayList<Movie>()
    lateinit var movieAdapter: MovieListAdapter
    lateinit var moviePageAdapter: MoviePageListAdapter

    override fun onViewDidCreate(savedInstanceState: Bundle?) {
        presenter = MainPresenter(this, this)
        apiList = RetrofitClient.instance.create(ApiInterfaces::class.java)
        setUpUi()
    }

    private fun setUpUi() {
        if (checkNetworkConnection(this)) {
            mainRecyclerView.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
            movieAdapter = MovieListAdapter(this, listMovie)
            mainRecyclerView.adapter = movieAdapter

            //below list init
            recyclerViewPageMovies.layoutManager = GridLayoutManager(this, 3)
            moviePageAdapter = MoviePageListAdapter(this, listMoviePage, this)
            recyclerViewPageMovies.adapter = moviePageAdapter
            getMoviesList()

            swipeRefresh.setOnRefreshListener {
                page = 2
                getMoviesList()
            }

        } else {
            Snackbar.make(
                main_layout,
                "Network not connected. Please reconnect and reopen application.",
                Snackbar.LENGTH_INDEFINITE
            ).show()
        }


    }

    private fun getMoviesList() {
        disposableList.add(presenter.getMoviesList(apiList))
        disposablePageList.add(presenter.getMoviesListAsPage(page, apiList))
    }

    override fun onMoviesListReady(movie: ArrayList<Movie>) {
        listMovie.addAll(movie)
        mainRecyclerView.adapter!!.notifyDataSetChanged()
    }

    override fun onErrorOccurAllList(message: String?) {
        Snackbar.make(main_layout, "$message", Snackbar.LENGTH_SHORT).show()
    }

    override fun onMoviesPageListReady(movie: ArrayList<Movie>) {
        swipeRefresh.isRefreshing = false
        listMoviePage.addAll(movie)
        recyclerViewPageMovies.adapter!!.notifyDataSetChanged()
//        recyclerViewPageMovies.scrollToPosition(listMoviePage.size - movie.size - 1)
    }

    override fun onErrorOccurPageList(message: String?) {
        Snackbar.make(main_layout, "$message", Snackbar.LENGTH_SHORT).show()
        swipeRefresh.isRefreshing = false
    }

    override fun onScrolledToBottom() {
        page++
        disposablePageList.add(presenter.getMoviesListAsPage(page, apiList))
    }

    override fun onDestroy() {
        super.onDestroy()
        disposablePageList.dispose()
        disposableList.dispose()
    }

}
