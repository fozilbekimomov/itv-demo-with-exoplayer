package uz.ictandroid.itv.ui.show

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import uz.ictandroid.itv.network.ApiInterfaces
import uz.ictandroid.itv.utils.token

/**
 * Created by Fozilbek on 6/13/2019 for iTV
 **/
class ShowPresenter(val view: ShowMvp.view) : ShowMvp.presenter {
    override fun getMovie(id: Long, apiList: ApiInterfaces): Disposable {
        return apiList.getMovieShowSingle(
            id,
            token

        ).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe({
            view.onMovieReady(it.data.movie)
            if (it.data.movie.files.snapshots != null)
                view.onSnapshotsReady(it.data.movie.files.snapshots)
        }, {
            view.onErrorOccured(it.message)
        })
    }

}