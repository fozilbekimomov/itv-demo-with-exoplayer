package uz.ictandroid.itv.ui.show

import io.reactivex.disposables.Disposable
import uz.ictandroid.itv.models.Movie
import uz.ictandroid.itv.models.Snapshot
import uz.ictandroid.itv.network.ApiInterfaces

/**
 * Created by Fozilbek on 6/13/2019 for iTV
 **/
interface ShowMvp {
    interface view {
        fun onMovieReady(movie: Movie)
        fun onSnapshotsReady(snapshots: ArrayList<Snapshot>)
        fun onErrorOccured(message: String?)
    }

    interface presenter {
        fun getMovie(id: Long, apiList: ApiInterfaces): Disposable
    }
}