package uz.ictandroid.itv.models

/**
 * Created by Fozilbek on 6/12/2019 for iTV
 **/
data class MovieListResponce(
    val code: Int,
    val message: String,
    val language: String,
    val subscription_status: String,
    val need_update: String,
    val data: MovieListData
)

data class MovieListData(
    val total_items: Int,
    val items_per_page: Int,
    val movies: ArrayList<Movie>
)