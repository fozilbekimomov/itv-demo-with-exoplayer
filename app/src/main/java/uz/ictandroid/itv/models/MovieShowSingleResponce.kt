package uz.ictandroid.itv.models

/**
 * Created by Fozilbek on 6/12/2019 in iTV
 **/
data class MovieShowSingleResponce(
    val code: Int,
    val message: String,
    val language: String,
    val subscription_status: String,
    val need_update: Boolean,
    val data: MovieShowData
)

data class MovieShowData(
    val movie: Movie
)