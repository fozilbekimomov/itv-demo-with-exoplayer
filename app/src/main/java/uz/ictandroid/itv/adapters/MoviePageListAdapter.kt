package uz.ictandroid.itv.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.support.v7.widget.AppCompatImageView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.RelativeLayout
import android.widget.TextView
import com.bumptech.glide.Glide
import uz.ictandroid.itv.R
import uz.ictandroid.itv.models.Movie
import uz.ictandroid.itv.ui.show.ShowActivity

/**
 * Created by Fozilbek on 6/12/2019 for iTV
 **/
class MoviePageListAdapter(
    val context: Context,
    val movies: ArrayList<Movie>, val onScrolledToBottom: OnScrolledToBottom?
) : RecyclerView.Adapter<MoviePageListAdapter.ViewHolder>() {
    var positions = ArrayList<Int>()

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder =
        ViewHolder(
            LayoutInflater.from(context).inflate(R.layout.item_movie_page, p0, false)
        )

    override fun getItemCount(): Int {
        return when (movies.size) {
            0 -> 18
            else -> movies.size
        }
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        if (movies.size != 0) {

            val movie = movies[position]
            holder.textViewTitle.setBackgroundColor(Color.TRANSPARENT)
            holder.textViewTitle.text = movie.title
            holder.textViewGenre.text = movie.genres_str
            holder.textViewYear.text = "${movie.year}"
            holder.textViewGenre.setBackgroundColor(Color.TRANSPARENT)
            Glide.with(context).load(movie.files.poster_url).into(holder.imageViewPoster)
            if (movie.params.is_hd) {
                holder.rvHd.visibility = View.VISIBLE
            } else {
                holder.rvHd.visibility = View.GONE
            }

            holder.itemView.setOnClickListener {
                val intent = Intent(context, ShowActivity::class.java)
                intent.putExtra("id", movie.id)
                context.startActivity(intent)
                if (onScrolledToBottom == null) {
                    (context as ShowActivity).finish()
                }
            }
            if (!positions.contains(position)) {
                positions.add(position)
                setScaleAnimation(holder.itemView)
            }

        }

//        setScaleAnimation(holder.itemView)

        if (position == movies.size - 1) {
            onScrolledToBottom?.onScrolledToBottom()
        }

    }

    private fun setScaleAnimation(view: View) {
        val anim = AnimationUtils.loadAnimation(context, R.anim.scale_up)
        view.startAnimation(anim)
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val textViewTitle = view.findViewById<TextView>(R.id.textViewTitle_page)
        val textViewGenre = view.findViewById<TextView>(R.id.textViewGenre_page)
        val imageViewPoster = view.findViewById<AppCompatImageView>(R.id.imageViewPoster_page)
        val rvHd = view.findViewById<RelativeLayout>(R.id.rvHD_page)
        val textViewYear = view.findViewById<TextView>(R.id.textViewYear_page)
    }

    interface OnScrolledToBottom {
        fun onScrolledToBottom()
    }

}