package uz.ictandroid.itv.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.support.v7.widget.AppCompatImageView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.RelativeLayout
import android.widget.TextView
import com.bumptech.glide.Glide
import uz.ictandroid.itv.R
import uz.ictandroid.itv.models.Movie
import uz.ictandroid.itv.ui.show.ShowActivity
import uz.ictandroid.itv.utils.logi

/**
 * Created by Fozilbek on 6/12/2019 for iTV
 **/
class MovieListAdapter(
    val context: Context, val movies: ArrayList<Movie>
) :
    RecyclerView.Adapter<MovieListAdapter.ViewHolder>() {
    var positions = ArrayList<Int>()
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder =
        ViewHolder(
            LayoutInflater.from(context).inflate(R.layout.item_movie, p0, false)
        )

    override fun getItemCount(): Int {
        return when (movies.size) {
            0 -> 10
            else -> movies.size
        }
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        if (movies.size != 0) {

            val movie = movies[position]
            holder.textViewTitle.setBackgroundColor(Color.TRANSPARENT)
            holder.textViewTitle.text = movie.title
            holder.textViewGenre.text = movie.genres_str
            holder.textViewYear.text = "${movie.year}"
            holder.textViewGenre.setBackgroundColor(Color.TRANSPARENT)
            Glide.with(context).load(movie.files.poster_url).into(holder.imageViewPoster)
            if (movie.params.is_hd) {
                holder.rvHd.visibility = View.VISIBLE
            } else {
                holder.rvHd.visibility = View.GONE
            }

            holder.itemView.setOnClickListener {
                val intent = Intent(context, ShowActivity::class.java)
                logi(this,"Movie id ${movie.id}")
                intent.putExtra("id", movie.id)
                context.startActivity(intent)

            }
            if (!positions.contains(position)) {
                positions.add(position)
                setScaleAnimation(holder.itemView)
            }
        }



    }

    private fun setScaleAnimation(view: View) {
        val anim = AnimationUtils.loadAnimation(context, R.anim.scale_up)
        view.startAnimation(anim)
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val textViewTitle = view.findViewById<TextView>(R.id.textViewTitle)
        val textViewGenre = view.findViewById<TextView>(R.id.textViewGenre)
        val imageViewPoster = view.findViewById<AppCompatImageView>(R.id.imageViewPoster)
        val rvHd = view.findViewById<RelativeLayout>(R.id.rvHD)
        val textViewYear = view.findViewById<TextView>(R.id.textViewYear)
    }


}