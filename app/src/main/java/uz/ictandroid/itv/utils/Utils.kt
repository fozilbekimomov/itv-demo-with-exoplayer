package uz.ictandroid.itv.utils

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.util.Log


/**
 * Created by Fozilbek on 6/12/2019 in ITVuz
 **/
val BASE_URL = "https://api.itv.uz"
val token = "87c7a2bb497d5647751ad33e6942bbfa"
val authorization = "user"
val MAX_PER_PAGE = 18

fun logi(any: Any, msg: Any) {
    Log.i("myLog $msg", "$msg")
}

fun checkNetworkConnection(context: Context): Boolean {
    var connected = false
    try {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        connected =
            connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).state == NetworkInfo.State.CONNECTED ||
                    connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).state == NetworkInfo.State.CONNECTED

    } catch (e: Exception) {
        logi("checkInternet Exception:", e)
    }
    return connected
}